# Portfolio

__Hey, my name's Raphael. Thanks for checking out my portfolio!__ <br>
In case you wish to know more about the projects I've worked on so far, feel free to check out the following links.

[My First App](https://gitlab.com/TenebrisAnima/kotlinproject)

[First Step into Spring Boot](https://gitlab.com/TenebrisAnima/springboot_vue)

[Bomberman AI](https://gitlab.com/TenebrisAnima/raphael-said-bomberman)

<hr>

## My personal strongpoints:

### Java

+ OOP
+ Algorithms
+ Spring Boot
+ REST
+ JPA
+ Hibernate

### Kotlin

+ Android Studio
+ Compose for desktop
+ Persistence
+ Ktor


### Project Management
+ Scrum
+ Requirements Engineering
+ Version Control via Gitlab
